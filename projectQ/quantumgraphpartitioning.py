import numpy
import igraph

from projectq import MainEngine
from projectq.backends import Simulator, CircuitDrawer
from projectq.meta import Control, Loop
from projectq.ops import (QubitOperator, TimeEvolution, Measure, All, H)


def graphViz(graph,name):

	' function for generating graphviz file '

	N = graph.vcount()

	with open("graph_" + name + str(N) + ".gv", "w") as file_graphviz:

		file_graphviz.write("# GraphViz configuration - " + name + " (size " + str(N) + ")\n")
		file_graphviz.write("graph {\n\tranksep=3;\n\tratio=auto;\n")
		file_graphviz.write("\n\t#TITLE\n\tlabelloc=\"t\";\n\tlabel=\"%s (size=%u)\";\n" % ( name , N ) );

		file_graphviz.write("\n\t# NODES\n")
		for i in range(0,N):
			file_graphviz.write("\tN%02u [ color=\"#000000\", fillcolor=\"%s\", shape=\"circle\", style=\"filled,solid\" ];\n" % \
					( i , "#66ffcc" if graph.vs[i]["partition"] else "#ff6600") )

		file_graphviz.write("\n\t# EDGES\n")
		for i in range(0,N):
			for j in range(i,N):
				if graph.are_connected(graph.vs[i],graph.vs[j]):
					if graph.vs[i]["partition"] == graph.vs[j]["partition"]:
						linetype = "solid"
						linecolor = "#338866" if 1 else "#884400"
					else:
						linetype = "dashed"
						linecolor = "#000000"

					file_graphviz.write("\tN%02u -- N%02u [ penwidth=2, style=\"%s\", color=\"%s\" ];\n" % \
							( i , j , linetype , linecolor ) )

		file_graphviz.write("\n}")
		print "\n>> GraphViz file created: " + file_graphviz.name

def circuitViz(graph,name):

	N = graph.vcount()

	' function for creating the latex visualization of the quantum circuit '
	drawer = CircuitDrawer()
	eng_latex = MainEngine(drawer)
	quantumPartitioning(eng_latex,graph,1,1) # one timestep only
	with open("circuit_" + name + str(N) + ".tex", "w") as file_latex:
		file_latex.write(drawer.get_latex())


def quantumPartitioning(eng,graph,T,dt):

	N = graph.vcount() # number of vertices
	E = graph.ecount() # number of edges
	D = graph.maxdegree() # maximum degree

	f = 1.6 # just some factor >= 1.
	h_0 = 1.
	B = h_0
	A = f * B/8. * min(2*D,N)

	# Constructing Hamiltonians

	# H_0: transverse magnetic field
	H_0 = 0 * QubitOperator()
	for i in range(0,N):
		H_0 += QubitOperator(((i,'X'),),-h_0)

	# H_P: artificial hamiltonian for graph partitioning
	H_P = B*E * QubitOperator()
	for i in range(0,N):
		for j in range(0,N):
			K_ij = A-.5*B if graph.are_connected(graph.vs[i],graph.vs[j]) else A
			H_P += QubitOperator(((i,'Z'),(j,'Z')),K_ij)

	# print "\n### H_0:\n", H_0
	# print "\n### H_P:\n", H_P

	# prepare ground state of H_0
	wavefunction = eng.allocate_qureg(N)
	All(H) | wavefunction

	# Evolve with mixed Hamiltonian H(t) = (1-t/T) H_0 + t/T H_P
	M = int(T/dt)
	C = numpy.linspace(0.,1.,M+1)
	for c in C:
		# print 100.*c/C[-1], "%"

		# first order
		TimeEvolution( dt, (1.-c)*H_0 ) | wavefunction
		TimeEvolution( dt, (c   )*H_P ) | wavefunction

		# second order
		# TimeEvolution( dt/2., (1.-c)*H_0 ) | wavefunction
		# TimeEvolution(    dt, (c   )*H_P ) | wavefunction
		# TimeEvolution( dt/2., (1.-c)*H_0 ) | wavefunction

	Measure | wavefunction

	eng.flush()

	graph.vs["partition"] = [int(qubit) for qubit in wavefunction]


if __name__ == "__main__":

	# note: larger graphs may require larger T and smaller dt
	N = 8
	T = 10.0
	dt = 0.01

	# constructing the graph
	# graph = igraph.Graph.Full(N); name = "full"
	# graph = igraph.Graph.Ring(N); name = "ring"
	# graph = igraph.Graph.Tree(N,1); name = "linear"
	# graph = igraph.Graph.Tree(N,2); name = "tree"
	graph = igraph.Graph.GRG(N,.45); name = "random"

	# graph = igraph.Graph(N); name = "custom"
	# graph.add_edges([(0,1),(0,2),(0,3),(1,2),(1,3),(2,3),(4,5),(5,6),(6,7),(7,4),(3,4)])

	# print graph

	# perform graph partitioning
	eng = MainEngine()
	quantumPartitioning(eng,graph,T,dt)
	print graph.vs["partition"]
	# circuitViz(graph,name)
	graphViz(graph,name)
